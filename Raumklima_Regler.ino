// Raumklimaregler für Kellerräume
//
// (C) Dr. Matthias Offer
// Juli 2013
// Version 1.xMEGA


// Laden der System-Bibliotheken
// -----------------------------------------------------------------------
#include "DHT.h"
#include "MATH.h"
#include "Wire.h" 
#include "LiquidCrystal_I2C.h"
#include "RTClib.h"
#include "SD.h"


// Globale Systemvariablen
// -----------------------------------------------------------------------
#define disk1 0x50                    // Addresse des 24LC64 EEPROM Chips
#define DHTTYPE DHT22                 // Typ der Sensoren -> DHT 22  (AM2302)
#define DHTPIN1 4                     // DigitalPIN des Innensensors
#define DHTPIN2 5                     // DigitalPIN des Aussensensors
#define RelaisPIN1 6                  // DigitalPIN des Relais 1
#define RelaisPIN2 7                  // DigitalPIN des Relais 2
#define Displaydelay 3                // Anzeigedauer in s von normalen Displayinformationen
float delta_tau;                      // kleinste Taupunktabweichung die noch erlaubt ist
float Hum_Offset;                     // Abgleich der beiden Sensoren
float Temp_Offset;                    // Abgleich der beiden Sensoren

boolean StatusInnensensor;            // Status des Innensensors
boolean StatusAussensensor;           // Status des Aussensensors
boolean StatusSDKarte;                // Status der SD-Karte
boolean toSave = false;               // True = es wird auf die SD-Karte gespeichert
boolean inputstringComplete = false;  // Ist der Eingabestring komplett?

String inputString = "";              // Variable für ankommende Befehle

unsigned int Save_delay;              // SD-Karten Speicherintervall in Sekunden
unsigned int WarteSekunden;           // Zeit Puffer für die Displayrotation

File dataFile;                        // Objekt für den Dateinamen
char FileName[] = "datalog.txt";      // Dateiname für den Datenlogger

DHT dhtInnen(DHTPIN1, DHTTYPE);       // Objekt für den Sensor
DHT dhtAussen(DHTPIN2, DHTTYPE);      // Objekt für den Sensor

RTC_DS1307 RTC;                       // Typ der RTC

LiquidCrystal_I2C lcd(0x27,16,2);     // LCD-Addresse: 0x27 16x2 Zeichen


// Schreibt 2Byte auf das EEPROM
// -----------------------------------------------------------------------
void writeEEPROM(int deviceaddress, unsigned int eeaddress, int data ) 
{
  byte lowByte = ((data >> 0) & 0xFF);     // Byte 1
  byte highByte = ((data >> 8) & 0xFF);    // Byte 2
  
  // Schreibt Byte 1
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));       // MSB1
  Wire.write((int)(eeaddress & 0xFF));     // LSB1
  Wire.write(lowByte);
  Wire.endTransmission();
  
  delay(5);
  
  // Schreibt Byte 2
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress +1 >> 8));    // MSB2
  Wire.write((int)(eeaddress +1 & 0xFF));  // LSB2
  Wire.write(highByte);
  Wire.endTransmission();
 
  delay(5);
}


// Liest ein Byte aus dem EEPROM
// -----------------------------------------------------------------------
byte readEEPROM(int deviceaddress, unsigned int eeaddress ) 
{
  byte rdata = 0xFF;
 
  Wire.beginTransmission(deviceaddress);
  Wire.write((int)(eeaddress >> 8));      // MSB
  Wire.write((int)(eeaddress & 0xFF));    // LSB
  Wire.endTransmission();
 
  Wire.requestFrom(deviceaddress,1);
 
  if (Wire.available()) rdata = Wire.read();
  
  delay(5);
 
  return rdata;
}


// Liest einen String über RS232 ein
// -----------------------------------------------------------------------
void serialEvent() {
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (!(inChar == '\n')) {
      if (!(inChar =='\r')){
        inputString += inChar;    
      }
    }
    else {
      inputstringComplete = true;
    }
  }
}


// Setzt die String Variable zurück
// -----------------------------------------------------------------------
void ClearInputStr() {
  inputString = "";
  inputstringComplete = false;
}


// Stellt den Namen und die Version des Reglers auf dem Display da
// -----------------------------------------------------------------------
void VersionInfo(){
  lcd.clear();
  lcd.print(F("Raumklimaregler"));
  lcd.setCursor(0, 1);
  lcd.print(F("Vers:1.x M.Offer"));
  delay (Displaydelay * 1000);
}


void Savedelay_Info(){
  lcd.clear();
  lcd.print(F("SD Log-Intervall"));
  lcd.setCursor(0, 1);
  lcd.print(F("Time= "));
  lcd.print(Save_delay);
  lcd.print(F(" sec"));
  delay (Displaydelay * 1000);
}


void delta_tau_Info(){
  lcd.clear();
  lcd.print(F("Regler-Settings:"));
  lcd.setCursor(0, 1);
  lcd.print(F("dTau= "));
  lcd.print(delta_tau);
  lcd.write(223);
  lcd.print(F("C"));
  delay (Displaydelay * 1000);
}


void Hum_Offset_Info(){
  lcd.clear();
  lcd.print(F("Regler-Settings:"));
  lcd.setCursor(0, 1);
  lcd.print(F("dH= "));
  lcd.print(Hum_Offset);
  lcd.print(F("%"));
  delay (Displaydelay * 1000);
}


void Temp_Offset_Info(){
  lcd.clear();
  lcd.print(F("Regler-Settings:"));
  lcd.setCursor(0, 1);
  lcd.print(F("dT= "));
  lcd.print(Temp_Offset);
  lcd.write(223);
  lcd.print(F("C"));
  delay (Displaydelay * 1000);
}


// Stellt das aktuelle Datum und die Uhrzeit auf dem Display da
// -----------------------------------------------------------------------
void LCD_Display_Time(String DATE, String TIME) {
  lcd.clear();
  lcd.print(F("Datum:"));
  lcd.print(DATE);
  lcd.setCursor(0, 1);
  lcd.print(F("Zeit :"));
  lcd.print(TIME);
}


// Stellt die aktuelle Temperatur auf dem Display da
// -----------------------------------------------------------------------
void LCD_Display_Temp(float Temp){
    lcd.setCursor(0, 1);
    lcd.print(F("Temp= "));
    lcd.print(Temp);
    lcd.write(223);
    lcd.print(F("C"));
}


// Stellt die aktuelle Luftfeuchte auf dem Display da
// -----------------------------------------------------------------------
void LCD_Display_Hum(float Hum){
    lcd.setCursor(0, 1);
    lcd.print(F("Feuchte= "));
    lcd.print(Hum);
    lcd.print(F("%"));
}


// Stellt den aktuellen Taupunkt auf dem Display da
// -----------------------------------------------------------------------
void LCD_Display_tau(float tau){
    lcd.setCursor(0, 1);
    lcd.print(F("Tau= "));
    lcd.print(tau);
    lcd.write(223);
    lcd.print(F("C"));
}


// Wahl der 3 Lüfterbetriebsarten
// -----------------------------------------------------------------------
void Set_Luefter_Mode(int Luefter_Mode){
  switch(Luefter_Mode){
    case 0:                            // Der Lüfter ist AUS
      digitalWrite(RelaisPIN1, HIGH);
      digitalWrite(RelaisPIN2, HIGH);
      break;
    case 1:                            // Belüftung von Draussen
      digitalWrite(RelaisPIN1, HIGH);
      digitalWrite(RelaisPIN2, LOW);
      break;
  }
}


// Berechnung des Taupunktes in Grad Celsius nach der Magnusformel
// -----------------------------------------------------------------------
float tau(float h, float t){
  float result = (241.2 * log(h / 100) + 4222.03716 * t / ( 241.2 + t)) / (17.5043 - log(h / 100) - (17.5043 * t / (241.2 + t)));
  return result;
}


// Hält das Programm an wenn es zu einem Fehler bei der Messdatei gekommen ist.
void SD_File_Error(){
  if (!dataFile) {
    lcd.clear();
    lcd.print(F("Fehler bei"));
    lcd.setCursor(0, 1);
    lcd.print(F("Messdatei!"));
      
    delay(Displaydelay * 1000);
      
    lcd.clear();
    lcd.print(F("Programm"));
    lcd.setCursor(0, 1);
    lcd.print(F("angehalten!"));
    
    while (1) ;
    }
}


// Initialisiert die SD-Karte und erstellt die Messdatei
// -----------------------------------------------------------------------
boolean Init_SDCard() {
  // Status der Initialisierung 0 = Fehler, 1 = OK
  boolean SD_Status = true;                
  
  pinMode(SS, OUTPUT);
  
  lcd.clear();
  lcd.print(F("Initialisiere"));
  lcd.setCursor(0, 1);
  lcd.print(F("SD-Karte..."));  
  
  delay(Displaydelay * 1000); 
  
  if (!SD.begin(10,11,12,13)) {
    // Die Initialisierung der SD-Karte ist fehlgeschlagen.
    SD_Status = false;
    
    lcd.clear();
    lcd.print(F("SD-Karte nicht"));
    lcd.setCursor(0, 1);
    lcd.print(F("gefunden!"));
    
    delay(Displaydelay * 1000);
    
    lcd.clear();
    lcd.print(F("Datenlogging"));
    lcd.setCursor(0, 1);
    lcd.print(F("ist deaktiviert!"));
    
    delay(Displaydelay * 1000);
  }
  if (SD_Status == true) {
    // Initialisierung hat geklappt. Messdatei wird erstellt.    
    lcd.clear();
    lcd.print(F("SD-Karte"));
    lcd.setCursor(0, 1);
    lcd.print(F("initialisiert!"));
    
    delay(Displaydelay * 1000);
    
    if (SD.exists(FileName)) {
      dataFile = SD.open(FileName, FILE_WRITE);
      
      SD_File_Error();
    }
    else {
      dataFile = SD.open(FileName, FILE_WRITE);
      
      SD_File_Error();

      dataFile.println(F("Belüftungsregler für Kellerräume"));
      dataFile.println(F("(C) Dr. Matthias Offer"));
      dataFile.println();
      dataFile.println(F("Spalte 1: Datum"));
      dataFile.println(F("Spalte 2: Uhrzeit"));
      dataFile.println(F("Spalte 3: Zeitstempel(s) (Unix-Time)"));
      dataFile.println(F("Spalte 4: Innen-Luftfeuchte(%)"));
      dataFile.println(F("Spalte 5: Innen-Temperatur(°C)"));
      dataFile.println(F("Spalte 6: Innen-Taupunkt(°C)"));
      dataFile.println(F("Spalte 7: Aussen-Luftfeuchte(%)"));
      dataFile.println(F("Spalte 8: Aussen-Temperatur(°C)"));
      dataFile.println(F("Spalte 9: Aussen-Taupunkt(°C)"));
      dataFile.println();
      dataFile.println(F("-------------------------------------------------------------------------------------"));
      dataFile.println();
      dataFile.flush();
    }
            
    lcd.clear();
    lcd.print(F("Datenlogging"));
    lcd.setCursor(0, 1);
    lcd.print(F("ist aktiviert!"));
    
    delay(Displaydelay * 1000);
  }
  return SD_Status;
}


// Stellt das aktuelle Datum als String da
// -----------------------------------------------------------------------
String Date_String(int day, int month, int year) {
  String DATE;
  String DAY;
  String MONTH;
  String YEAR;
  
  if (day < 10){DAY="0"+String(day, DEC);}
  else{DAY=String(day, DEC);}
  
  if (month < 10){MONTH = "0"+String(month, DEC);}
  else{MONTH = String(month, DEC);}

  YEAR=String(year, DEC);
  
  DATE = DAY+"."+MONTH+"."+YEAR;
  
  return DATE;
}


// Stellt die aktuelle Uhrzeit als String da
// -----------------------------------------------------------------------
String Time_String(int hour, int minute) {
  String TIME;
  String HOUR;
  String MINUTE;
  
  if (hour < 10){HOUR="0"+String(hour, DEC);}
  else{HOUR=String(hour, DEC);}
  
  if (minute < 10){MINUTE = "0"+String(minute, DEC);}
  else{MINUTE = String(minute, DEC);}
  
  TIME = HOUR+":"+MINUTE;
  
  return TIME;
}


// Setup des Programms
// -----------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  Wire.begin();
  RTC.begin();
  dhtInnen.begin();
  dhtAussen.begin();
  
  pinMode(RelaisPIN1, OUTPUT);     // DigtalPIN des Relais 1 wird als Ausgang definiert
  pinMode(RelaisPIN2, OUTPUT);     // DigtalPIN des Relais 2 wird als Ausgang definiert
    
  digitalWrite(RelaisPIN1, HIGH);  // DigtalPIN des Relais 1 wird auf HIGH gesetzt -> Relaisspule ist stromlos
  digitalWrite(RelaisPIN2, HIGH);  // DigtalPIN des Relais 2 wird auf HIGH gesetzt -> Relaisspule ist stromlos
    
  lcd.init();                      // Initialisiert das LCD-Display
  lcd.backlight();                 // Intergrundbeleuchtung ist AN
 
  VersionInfo();                   // Stellt den Namen und die Version des Reglers auf dem Display da
  
  Save_delay =  (int)(((readEEPROM(disk1, 0) << 0) & 0xFF) + ((readEEPROM(disk1, 1) << 8) & 0xFF00));
  
  Hum_Offset =  (float)((int)(((readEEPROM(disk1, 2) << 0) & 0xFF) + ((readEEPROM(disk1, 3) << 8) & 0xFF00)))/100;
  Temp_Offset = (float)((int)(((readEEPROM(disk1, 4) << 0) & 0xFF) + ((readEEPROM(disk1, 5) << 8) & 0xFF00)))/100;
  delta_tau =   (float)((int)(((readEEPROM(disk1, 6) << 0) & 0xFF) + ((readEEPROM(disk1, 7) << 8) & 0xFF00)))/100;
  
  lcd.clear();
  lcd.print(F("Lade vom EEPROM!"));
  delay (Displaydelay * 1000);
  Savedelay_Info();                // Stellt das Speicherinterval in Sekunden auf dem Display da
  lcd.clear();
  Hum_Offset_Info();
  lcd.clear();
  Temp_Offset_Info();
  lcd.clear();
  delta_tau_Info();
  
  StatusSDKarte = Init_SDCard();   // Initialisiert die SD-Karte und erstellt die Messdatei 0 = SD-Karte fehlt 1 = OK
  
  // Stellt die Echtzeituhr beim Upload des Programms auf die Rechnerzeit
  if (! RTC.isrunning()) {
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
}


// Begin der Programmschleife
// -----------------------------------------------------------------------
void loop() {
  DateTime now = RTC.now();
  
  String Date = Date_String(now.day(), now.month(), now.year());
  String Time = Time_String(now.hour(), now.minute());
  
  unsigned long uTime = now.unixtime();
  unsigned int Sekunde = now.second();

  float HumInnen = dhtInnen.readHumidity() + Hum_Offset;
  float TempInnen = dhtInnen.readTemperature() + Temp_Offset;
  
  float HumAussen = dhtAussen.readHumidity();
  float TempAussen = dhtAussen.readTemperature();
  
  float tauInnen;                     // Taupunkt am Ort des Innensensors
  float tauAussen;                    // Taupunkt am Ort des Aussensensors
  
  if (Sekunde % (Displaydelay*8) == 0) {
    LCD_Display_Time(Date, Time);
    WarteSekunden = Sekunde;
  }

  if (isnan(TempInnen) || isnan(HumInnen)) {
    if (Sekunde == WarteSekunden + Displaydelay) {
      lcd.clear();
      lcd.print(F("Fehler beim"));
      lcd.setCursor(0, 1);
      lcd.print(F("Innensensor!"));
    }
    StatusInnensensor = false;
  } 
    
  else {
    tauInnen = tau(HumInnen, TempInnen);
    
    if (Sekunde == WarteSekunden + Displaydelay) {
      lcd.clear();
      lcd.print(F("Innen"));
      LCD_Display_Temp(TempInnen);
    }
    
    if (Sekunde == WarteSekunden + 2 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Innen"));
      LCD_Display_Hum(HumInnen);
    }
    
    if (Sekunde == WarteSekunden + 3 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Innen"));
      LCD_Display_tau(tauInnen);
    }
    
    StatusInnensensor = true;  
  }
  
  if (isnan(TempAussen) || isnan(HumAussen)) {
    if (Sekunde == WarteSekunden + 4 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Fehler beim"));
      lcd.setCursor(0, 1);
      lcd.print(F("Aussensensor!"));
    }
    
    StatusAussensensor = false;
  } 
    
  else {
    tauAussen = tau(HumAussen, TempAussen);
    
    if (Sekunde == WarteSekunden + 4 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Aussen"));
      LCD_Display_Temp(TempAussen);
    }
    
    if (Sekunde == WarteSekunden + 5 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Aussen"));
      LCD_Display_Hum(HumAussen);
    }
    
    if (Sekunde == WarteSekunden + 6 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Aussen"));
      LCD_Display_tau(tauAussen);
    }
    
    StatusAussensensor = true;
  }
  
  
  // Schaltet den Lüfter in Abhängikeit des Taupunktes
  // -----------------------------------------------------------------------  
  if (StatusInnensensor && StatusAussensensor == true) {
    if (tauAussen < tauInnen - delta_tau) {
      Set_Luefter_Mode(1);    // Belüftung von Draussen
      if (Sekunde == WarteSekunden + 7 * Displaydelay) {
        lcd.clear();
        lcd.print(F("Aussenbel"));
        lcd.write(245);
        lcd.print(F("ftung"));
        lcd.setCursor(0, 1);
        lcd.print(F("ist AKTIV!"));
      }
    }
    else {
      Set_Luefter_Mode(0);    // Der Lüfter ist AUS
      if (Sekunde == WarteSekunden + 7 * Displaydelay) {
        lcd.clear();
        lcd.print(F("Aussenbel"));
        lcd.write(245);
        lcd.print(F("ftung"));
        lcd.setCursor(0, 1);
        lcd.print(F("ist AUS!"));
      }
    }
  }
  else {
    Set_Luefter_Mode(0);      // Der Lüfter ist AUS
    if (Sekunde == WarteSekunden + 7 * Displaydelay) {
      lcd.clear();
      lcd.print(F("Aussenbel"));
      lcd.write(245);
      lcd.print(F("ftung"));
      lcd.setCursor(0, 1);
      lcd.print(F("ist AUS!"));
    }
  }
  
  
  // Speichert alle "Save_delay" Minuten die Messwerte auf die SD-Karte wenn kein Fehler aufgetreten ist
  // -----------------------------------------------------------------------
  if (uTime % Save_delay == 0) {
    if (toSave == true) {
      if (StatusSDKarte == true) {
        dataFile.print(Date);
        dataFile.print("\t");
        dataFile.print(Time);
        dataFile.print("\t");
        dataFile.print(uTime);
        dataFile.print("\t");
        dataFile.print(HumInnen);
        dataFile.print("\t");
        dataFile.print(TempInnen);
        dataFile.print("\t");
        dataFile.print(tauInnen);
        dataFile.print("\t");
        dataFile.print(HumAussen);
        dataFile.print("\t");
        dataFile.print(TempAussen);
        dataFile.print("\t");
        dataFile.println(tauAussen);
        dataFile.flush();
      }
      toSave = false;
    }
  }
  else {
    toSave = true;
  }
  
  serialEvent();
  
  if (inputstringComplete) {

    //command: get:init
    if (inputString=="get:init"){
      Serial.println(F("Raumklimaregler V1.xMEGA (C) Dr. Matthias Offer ok"));
    }
    //command: get:huminnen
    else if (inputString=="get:huminnen"){
      Serial.println(HumInnen);
    }
    //command: get:humaussen
    else if (inputString=="get:humaussen"){
      Serial.println(HumAussen);
    }
    //command: get:tempinnen
    else if (inputString=="get:tempinnen"){
      Serial.println(TempInnen);
    }
    //command: get:tempaussen
    else if (inputString=="get:tempaussen"){
      Serial.println(TempAussen);
    }
    //command: get:tauinnen
    else if (inputString=="get:tauinnen"){
      Serial.println(tauInnen);
    }
    //command: get:tauaussen
    else if (inputString=="get:tauaussen"){
      Serial.println(tauAussen);
    }
    //command: get:alldata
    else if (inputString=="get:alldata"){
      Serial.print(uTime);
      Serial.print(" ");
      Serial.print(Date_String(now.day(), now.month(), now.year()));
      Serial.print(" ");
      Serial.print(Time_String(now.hour(), now.minute()));
      Serial.print(" ");
      Serial.print(HumInnen);
      Serial.print(" ");
      Serial.print(TempInnen);
      Serial.print(" ");
      Serial.print(tauInnen);
      Serial.print(" ");
      Serial.print(HumAussen);
      Serial.print(" ");
      Serial.print(TempAussen);
      Serial.print(" ");
      Serial.print(tauAussen);
      Serial.print(" ");
      Serial.print(Save_delay);
      Serial.print(" ");
      Serial.print(Hum_Offset);
      Serial.print(" ");
      Serial.print(Temp_Offset);
      Serial.print(" ");
      Serial.println(delta_tau);
    }
    //command: get:date
    else if (inputString=="get:date"){
      Serial.println(Date_String(now.day(), now.month(), now.year()));
    }
    //command: get:time
    else if (inputString=="get:time"){
      Serial.println(Time_String(now.hour(), now.minute()));
    }
    //command: get:timestamp
    else if (inputString=="get:timestamp"){
      Serial.println(uTime);
    }
    //command: get:savedelay
    else if (inputString=="get:savedelay"){
      Serial.println(Save_delay);
    }
    //command: get:humoffset
    else if (inputString=="get:humoffset"){
      Serial.println(Hum_Offset);
    }
    //command: get:tempoffset
    else if (inputString=="get:tempoffset"){
      Serial.println(Temp_Offset);
    }
    //command: get:deltatau
    else if (inputString=="get:deltatau"){
      Serial.println(delta_tau);
    }
    //command: set:
    else if (inputString.startsWith("set:")){
      inputString=inputString.substring(4);
        //command: set:savedelayX X in Sekunden
        if (inputString.startsWith("savedelay")){
          inputString=inputString.substring(9);
          Save_delay=inputString.toInt();
          writeEEPROM(disk1, 0, Save_delay);
          Serial.println(Save_delay);
          Savedelay_Info();
        }
        //command: set:utimeXXXXXXXXXX X = Unix Zeitstempel als UTC/GMT
        if (inputString.startsWith("utime")){
          inputString=inputString.substring(5);
          if(inputString.length()>=8){
            RTC.adjust(DateTime(inputString.toInt()));
            Serial.println(inputString.toInt());
          }
        }
        //command: set:humoffsetX.X X.X = Luftfeuchteoffset
        if (inputString.startsWith("humoffset")){
          inputString=inputString.substring(9);
          char charBuf[5];
          inputString.toCharArray(charBuf, 5);
          Hum_Offset = atof(charBuf);
          writeEEPROM(disk1, 2, (int)(Hum_Offset * 100));
          Serial.println(Hum_Offset);
          Hum_Offset_Info();
        }
        //command: set:tempoffsetX.X X.X = Temperaturoffset
        if (inputString.startsWith("tempoffset")){
          inputString=inputString.substring(10);
          char charBuf[5];
          inputString.toCharArray(charBuf, 5);
          Temp_Offset = atof(charBuf);
          writeEEPROM(disk1, 4, (int)(Temp_Offset * 100));
          Serial.println(Temp_Offset);
          Temp_Offset_Info();
        }
        //command: set:deltatauX.X X.X = Delta Tau
        if (inputString.startsWith("deltatau")){
          inputString=inputString.substring(8);
          char charBuf[5];
          inputString.toCharArray(charBuf, 5);
          delta_tau = atof(charBuf);
          writeEEPROM(disk1, 6, (int)(delta_tau * 100));
          Serial.println(delta_tau);
          delta_tau_Info();
        }
      }
    else {
      Serial.println("command unknown");
    }
    
  ClearInputStr();
  }
  
  // Ende...
}
